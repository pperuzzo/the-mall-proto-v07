﻿using UnityEngine;
using System.Collections;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System;
using Oculus.Platform;
using Oculus.Platform.Models;
using System.Collections.Generic;

public class NetworkingEventHandler : MonoBehaviourPunCallbacks, IOnEventCallback
{

    #region Public Variables

    // Photon events codes
    public enum PhotonEventCodes
    {
        InstantiateAvatars = 1
    }

    #endregion

    #region Interface

    public void OnEvent(EventData photonEvent)
    {
       if (photonEvent.Code == (byte)PhotonEventCodes.InstantiateAvatars)
       {

            object[] data = photonEvent.CustomData as object[];
            // data[0] -> photonView.ViewID
            // data[1] -> oculusID
            // data[2] -> Oculus User Name

            GameObject remoteAvatar = Instantiate(Resources.Load("RemoteAvatar")) as GameObject;
            PhotonView photonView = remoteAvatar.GetComponent<PhotonView>();
            OvrAvatar ovrAvatar = remoteAvatar.GetComponent<OvrAvatar>();
            ovrAvatar.oculusUserID = data[1].ToString();

            photonView.ViewID = (int)data[0];


            RemoteUserData remoteUserToAdd = new RemoteUserData()
            {
                remoteUserID = data[1].ToString(),
                remoteUserName = data[2].ToString(),
                remoteAvatar = remoteAvatar,
                remoteOvrAvatar = ovrAvatar,
                photonViewID = (int)data[0]
            };
            Debug.Log(remoteUserToAdd.remoteUserID);

            Debug.Log(remoteUserToAdd.remoteUserName);

            NetworkingManager.Instance.allRemoteUsers.Add(remoteUserToAdd.remoteUserID, remoteUserToAdd);

            foreach (KeyValuePair<string, RemoteUserData> entry in NetworkingManager.Instance.allRemoteUsers)
            {
                Debug.Log(entry.Key);
            }

        }
    }

    #endregion

}

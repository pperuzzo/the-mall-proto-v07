﻿using UnityEngine;
using System.Collections;

public static class LocalUserData
{
    public static ulong localUserID;
    public static string localUserName;
    public static GameObject LocalAvatar;
    public static OvrAvatar localOvrAvatar;
    public static int photonViewID;
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System.Collections.Generic;

public class NetworkingManager : MonoBehaviourPunCallbacks
{
    #region Private Fields

    private readonly byte instantiateVrAvatarEventCode = 1;

    #endregion

    #region Public Fields 

    public Transform trackingSpace;

    public static NetworkingManager Instance;
    public Dictionary<string, RemoteUserData> allRemoteUsers = new Dictionary<string, RemoteUserData>();

    #endregion

    #region Public Methods

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void InstantiateAvatars()
    {
        GameObject localAvatar= Instantiate(Resources.Load("LocalAvatar")) as GameObject;
        localAvatar.transform.parent = trackingSpace.transform;

        // Get the reference to the LocalAvatar components
        PhotonView photonView = localAvatar.GetComponent<PhotonView>();
        OvrAvatar ovrAvatar = localAvatar.GetComponent<OvrAvatar>();

        // Set OculusID (for avatar skin) on the LocalAvatar
        ovrAvatar.oculusUserID = LocalUserData.localUserID.ToString();

        // Set references to LocalUserData
        LocalUserData.LocalAvatar = localAvatar;
        LocalUserData.localOvrAvatar = ovrAvatar;
        LocalUserData.photonViewID = photonView.ViewID;

        if (PhotonNetwork.AllocateViewID(photonView))
        {

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                CachingOption = EventCaching.AddToRoomCache,
                Receivers = ReceiverGroup.Others
            };

            SendOptions sendOptions = new SendOptions
            {
                Reliability = true
            };

            object[] dataForEvent = new object[3] { photonView.ViewID, LocalUserData.localUserID.ToString(), LocalUserData.localUserName };


            PhotonNetwork.RaiseEvent(instantiateVrAvatarEventCode, dataForEvent, raiseEventOptions, sendOptions);
        }
        else
        {
            Debug.LogError("Failed to allocate a ViewId.");

            Destroy(localAvatar);
        }
    }

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        Instance = this;
        InstantiateAvatars();
    }

    #endregion

    #region Photon Callbacks

    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting

    }

    public override void OnPlayerLeftRoom(Player other)
    {
        Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); // seen when other disconnects

        if (allRemoteUsers.ContainsKey(other.NickName))
        {
            RemoteUserData remoteUserLeaving = allRemoteUsers[other.NickName];

            allRemoteUsers.Remove(other.NickName);
            Debug.Log("Player removed because left room, now remaining:");
            foreach (KeyValuePair<string, RemoteUserData> entry in allRemoteUsers)
            {
                Debug.Log(entry.Key);
            }

        }

    }

    #endregion

 }

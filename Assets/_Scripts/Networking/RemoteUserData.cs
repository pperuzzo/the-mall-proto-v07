﻿using UnityEngine;


public class RemoteUserData
{
    public string remoteUserID;
    public string remoteUserName;
    public GameObject remoteAvatar;
    public OvrAvatar remoteOvrAvatar;
    public int photonViewID;
}


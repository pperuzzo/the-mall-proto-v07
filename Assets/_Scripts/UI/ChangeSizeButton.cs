﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeSizeButton : MonoBehaviour
{
    #region Public Fields

    public ProductData productData;
    public bool isButtonPressed = false;
    public Image imageComponent;
    public int size;

    public Sprite spriteClicked;
    public Sprite spriteNonClicked;
    public Image checkedImage;

    #endregion

    #region Public Methods

    public void ToggleSize()
    {
        foreach (ChangeSizeButton item in UIHandler.Instance.sizeElements.Values)
        {
            item.isButtonPressed = false;
            item.UpdateButton();
        }
        isButtonPressed = true;
        UpdateButton();

        DressingRoomManager.Instance.WearDifferentSize(productData, size);
    }

    public void UpdateButton()
    {
        if (isButtonPressed)
        {
            imageComponent.sprite = spriteClicked;
            checkedImage.enabled = true;
        }
        else
        {
            imageComponent.sprite = spriteNonClicked;
            checkedImage.enabled = false;
        }
    }

    #endregion

}

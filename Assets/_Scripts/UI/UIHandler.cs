﻿using UnityEngine;
using System.Collections;
using static Messages;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using System;
using VRTK;

public class UIHandler : MonoBehaviour
{
    #region Private Fields

    private int currentDisplayedProduct;

    #endregion

    #region Public Fields

    public static UIHandler Instance;
    public Cart cart;
    public DressingRoomManager dressingRoomManager;
    public Dictionary<string, GameObject> cartElements = new Dictionary<string, GameObject>();
    public Dictionary<int, ChangeColorButton> colorElements = new Dictionary<int, ChangeColorButton>();
    public Dictionary<int, ChangeSizeButton> sizeElements = new Dictionary<int, ChangeSizeButton>();


    [Header("Cart panel setup")]
    public Transform cartContainer;
    public GameObject cartElement;

    [Space(10)]

    [Header("Color panel setup")]
    public Transform colorContainer;
    public GameObject colorElement;

    [Space(10)]

    [Header("Size panel setup")]
    public Transform sizeContainer;
    public GameObject sizeElement;

    [Space(10)]

    [Header("ONLY THE MALL SCENE")]
    public VRTK_BasicTeleport teleporter;
    public Transform destinationPointTransform;

    [Space(10)]

    public TextMeshProUGUI[] detailsTexts = new TextMeshProUGUI[7];

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        Instance = this;
        if (cart != null)
        {
            cart.AddToCartEvent += OnAddToCart;
            cart.RemoveFromCartEvent += OnRemoveFromCart;
        }

        if (dressingRoomManager != null)
        {
            dressingRoomManager.WearProductEvent += OnWearProductEvent;
            dressingRoomManager.RemoveProductEvent += OnRemoveProductEvent;
        }

        if (teleporter != null)
        {
            teleporter.Teleported += OnTeleported;
        }


    }

    private void OnDisable()
    {
        if (cart != null)
        {
            cart.AddToCartEvent -= OnAddToCart;
            cart.RemoveFromCartEvent -= OnRemoveFromCart;
        }

        if (dressingRoomManager != null)
        {
            dressingRoomManager.WearProductEvent -= OnWearProductEvent;
            dressingRoomManager.RemoveProductEvent -= OnRemoveProductEvent;
            colorElements.Clear();
            sizeElements.Clear();
            foreach (TextMeshProUGUI item in detailsTexts)
            {
                item.text = "";
                item.enabled = false;
                item.enabled = true;
                item.text = " ";
            }

            foreach (Transform item in colorContainer.transform)
            {
                Destroy(item.gameObject);
            }

            foreach (Transform item in sizeContainer.transform)
            {
                Destroy(item.gameObject);
            }
        }

        if (teleporter != null)
        {
            teleporter.Teleported -= OnTeleported;
        }


    }

    #endregion

    #region Public Methods

    public void GenerateErrorUI(string errorKey)
    {
        /*
         * TODO
         * GENERATES UI ERROR / ACTIVATE PRE-CREATED GO
         * TODO
         */
        Debug.Log("Error: " + errorKey + "\n" + messageType[errorKey]);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    #endregion

    #region Private Methods

    private void InstantiateCartElement(ProductData product)
    {
        GameObject cartElementInstance = Instantiate(cartElement, cartContainer);
        float offset = cartElements.Count * 80f;
        cartElementInstance.transform.localPosition = new Vector3(0f, -offset, 0f);
        cartElements.Add(product.Id.ToString(), cartElementInstance);
        cartElementInstance.GetComponent<ProductContainer>().productData = product;
        TextMeshProUGUI[] cartElementTexts = cartElementInstance.GetComponentsInChildren<TextMeshProUGUI>();
        cartElementTexts[0].text = product.Title;
        cartElementTexts[1].text = "$" + product.Price;
        cartElementInstance.GetComponentInChildren<Image>().sprite = product.ProductImage;
        product.CartButtonReference = cartElementInstance.GetComponentInChildren<CartWearButton>();
    }

    private void RemoveCartElement(ProductData product)
    {
        if (cartElements.TryGetValue(product.Id.ToString(), out GameObject toBeDeleteElement))
        {
            cartElements.Remove(product.Id.ToString());
            Destroy(toBeDeleteElement);
            UpdateCartElements();
        }
    }

    private void UpdateCartElements()
    {
        int i = 0;
        foreach (var item in cartElements)
        {
            item.Value.transform.localPosition = new Vector3(0f, (-i * 80f), 0f);
            i++;
        }
    }

    private void AddInfoUI(DressingRoomManager.DressingEventArgs data)
    {
        currentDisplayedProduct = data.dataProduct.Id;
        colorElements.Clear();
        sizeElements.Clear();


        detailsTexts[0].text = data.dataProduct.Title;
        detailsTexts[1].text = data.dataProduct.Brand;
        detailsTexts[2].text = "$" + data.dataProduct.Price;
        detailsTexts[3].text = data.dataProduct.Description;
        detailsTexts[4].text = data.dataProduct.Material;
        detailsTexts[5].text = "Suggested Size";
        detailsTexts[6].text = ConvertSize(data.dataSize);

        // Setup color panel
        int i = 0;
        foreach (Color item in data.dataProduct.ProductColorVariants)
        {
            GameObject colorElementInstance = Instantiate(colorElement, colorContainer);
            int count = colorElements.Count;
            float offset = -0.4267f + (count * 0.1839f);
            colorElementInstance.transform.localPosition = new Vector3(offset, 0f, 0f);
            ChangeColorButton colorElementButton = colorElementInstance.GetComponentInChildren<ChangeColorButton>();
            colorElementButton.productData = data.dataProduct;
            colorElementButton.imageComponent.color = item;
            colorElementButton.buttonColor = item;
            colorElements.Add(i, colorElementButton);
            i++;
        }

        // Setup size panel
        int x = 0;
        foreach (var item in data.dataProduct.ToWearGameObject)
        {
            GameObject sizeElementInstance = Instantiate(sizeElement, sizeContainer);
            int count = sizeElements.Count;
            float offset = -0.4267f + (count * 0.1839f);
            sizeElementInstance.transform.localPosition = new Vector3(offset, 0f, 0f);
            ChangeSizeButton sizeElementButton = sizeElementInstance.GetComponentInChildren<ChangeSizeButton>();
            sizeElementInstance.GetComponentInChildren<TextMeshProUGUI>().text = ConvertSize(x.ToString());
            sizeElementButton.productData = data.dataProduct;
            sizeElementButton.size = x;
            sizeElements.Add(x, sizeElementButton);
            x++;

        }

        //Make the original color the active one
        colorElements[0].isButtonPressed = true;
        colorElements[0].UpdateButton();


        // Make the predicted size the active one.
        Int32.TryParse(data.dataSize, out int size);
        sizeElements[size].isButtonPressed = true;
        sizeElements[size].UpdateButton();

    }

    private void RemoveInfoUI(DressingRoomManager.DressingEventArgs data)
    {
        if (currentDisplayedProduct == data.dataProduct.Id)
        {
            colorElements.Clear();
            sizeElements.Clear();
            foreach (TextMeshProUGUI item in detailsTexts)
            {
                item.text = "";
                item.enabled = false;
                item.enabled = true;
                item.text = " ";
            }

            foreach (Transform item in colorContainer.transform)
            {
                Destroy(item.gameObject);
            }

            foreach (Transform item in sizeContainer.transform)
            {
                Destroy(item.gameObject);
            }
        }

    }

    private string ConvertSize(string dataSize)
    {
        Int32.TryParse(dataSize, out int size);
        switch (size)
        {
            case 0:
                return "S";
            case 1:
                return "M";
            case 2:
                return "L";
            default:
                return "Error";
        }
    }

    #endregion

    #region OnEvent

    // Updates UI on element Added to cart
    public void OnAddToCart(object sender, Cart.CartEventArgs e)
    {
        InstantiateCartElement(e.data);
    }

    // Updates UI on element Removed to cart
    public void OnRemoveFromCart(object sender, Cart.CartEventArgs e)
    {
        RemoveCartElement(e.data);
    }

    // Updates UI on weared product
    public void OnWearProductEvent(object sender, DressingRoomManager.DressingEventArgs e)
    {
        AddInfoUI(e);
    }

    // Updates UI on removed product
    public void OnRemoveProductEvent(object sender, DressingRoomManager.DressingEventArgs e)
    {
        RemoveInfoUI(e);
    }

    // Enables transfering UI if destination target is destination point in TheMall scene
    public void OnTeleported(object sender, DestinationMarkerEventArgs e)
    {
        if (e.target == destinationPointTransform)
        {
            Debug.Log("Display UI");
        }
    }

    #endregion

}

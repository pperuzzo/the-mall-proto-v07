﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeColorButton : MonoBehaviour
{
    #region Public Fields

    public ProductData productData;
    public bool isButtonPressed = false;
    public Image imageComponent;
    public Color buttonColor;

    public Sprite spriteClicked;
    public Sprite spriteNonClicked;

    public Image checkedImage;

    #endregion

    #region MonoBehaviour Callbacks

    private void OnDisable()
    {
        foreach (Material item in productData.ProductMaterials)
        {
            item.color = productData.ProductColorVariants[0];
        }
    }

    #endregion

    #region Public Methods

    public void ToggleColor()
    {
        foreach (ChangeColorButton item in UIHandler.Instance.colorElements.Values)
        {
            item.isButtonPressed = false;
            item.UpdateButton();
        }
        isButtonPressed = true;
        UpdateButton();

        foreach (Material item in productData.ProductMaterials)
        {
            item.color = buttonColor;
        }

    }

    public void UpdateButton()
    {
        if (isButtonPressed)
        {
            imageComponent.sprite = spriteClicked;
            imageComponent.color = buttonColor;
            checkedImage.enabled = true;
        }
        else
        {
            imageComponent.sprite = spriteNonClicked; 
            imageComponent.color = buttonColor;
            checkedImage.enabled = false;
        }
    }

    #endregion
}

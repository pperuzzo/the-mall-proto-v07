﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SetupHandColliders : MonoBehaviour
{
    //public VRTK_InteractTouch leftInteractTouch;
    //public VRTK_InteractTouch rightInteractTouch;
    public GameObject leftHandCollider;
    public GameObject rightHandCollider;

    private void Awake()
    {
        leftHandCollider = GameObject.Find("LeftHandColliders");
        rightHandCollider = GameObject.Find("RightHandColliders");

    }

    private void Update()
    {
        //GameObject leftIndex = VRTK_SharedMethods.FindEvenInactiveGameObject<OvrAvatarLocalDriver>("hand_left/hand_left_renderPart_0/hands:l_hand_world/hands:b_l_hand/hands:b_l_index1/hands:b_l_index2/hands:b_l_index3", true);
        //GameObject rightIndex = VRTK_SharedMethods.FindEvenInactiveGameObject<OvrAvatarLocalDriver>("hand_right/hand_right_renderPart_0/hands:r_hand_world/hands:b_r_hand/hands:b_r_index1/hands:b_r_index2/hands:b_r_index3", true);

        GameObject leftHand = VRTK_SharedMethods.FindEvenInactiveGameObject<OvrAvatarLocalDriver>("hand_left");
        GameObject rightHand = VRTK_SharedMethods.FindEvenInactiveGameObject<OvrAvatarLocalDriver>("hand_right");


        //if (leftIndex != null && rightIndex != null)
        if (leftHand != null && rightHand != null)
        {

            /*
           VRTK_TransformFollow leftTouchTransformFollow = 
               leftInteractTouch.controllerCollisionDetector.GetComponentInChildren<VRTK_TransformFollow>();

           VRTK_TransformFollow rightTouchTransformFollow =
               rightInteractTouch.controllerCollisionDetector.GetComponentInChildren<VRTK_TransformFollow>();

           leftTouchTransformFollow.gameObjectToFollow = leftIndex;
           rightTouchTransformFollow.gameObjectToFollow = rightIndex;
           */

            leftHandCollider.GetComponent<VRTK_TransformFollow>().gameObjectToFollow = leftHand;
            rightHandCollider.GetComponent<VRTK_TransformFollow>().gameObjectToFollow = rightHand;


            Destroy(GetComponent<SetupHandColliders>());
        }

    }

}

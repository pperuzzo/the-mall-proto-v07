﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CartWearButton : MonoBehaviour
{
    #region Private Fields

    private SizeCalculator sizeCalculator = new SizeCalculator();
    private int recommendedSize = 100;

    #endregion

    #region Public Fields

    public ProductContainer productContainer;
    public bool isButtonPressed = false;
    public Image imageComponent;

    public Sprite spriteClicked;
    public Sprite spriteNonClicked;
    public bool forceButton = false;

    #endregion

    #region Public Methods

    public void ToggleButton()
    {
        if (!isButtonPressed)
        {
            if (recommendedSize == 100)
            {
                recommendedSize = sizeCalculator.RecommendSize(DressingRoomManager.Instance.avatarData, productContainer.productData);
            }
            DressingRoomManager.Instance.ManagerWearProduct(productContainer.productData, recommendedSize);
            UpdateButton();
        }
        else
        {
            DressingRoomManager.Instance.RemoveProduct(productContainer.productData);
            UpdateButton();
        }

    }

    public void RemoveButton()
    {
        if (productContainer.productData.InstantiatedGameObject)
        {
            DressingRoomManager.Instance.RemoveProduct(productContainer.productData);
        }
        Cart.Instance.RemoveFromCart(productContainer.productData);
    }

    public void UpdateButton()
    {
        if (!forceButton)
        {
            if (isButtonPressed)
            {
                imageComponent.sprite = spriteNonClicked;
                imageComponent.color = Color.white;
                isButtonPressed = !isButtonPressed;
            }
            else
            {
                imageComponent.sprite = spriteClicked;
                imageComponent.color = Color.red;
                isButtonPressed = !isButtonPressed;
            }
        }
        else
        {
            ForceResetButton();
        }

    }

    public void ForceResetButton()
    {
        imageComponent.sprite = spriteNonClicked;
        imageComponent.color = Color.white;
        isButtonPressed = false;
        forceButton = false;
    }

    #endregion
}

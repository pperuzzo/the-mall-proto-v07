﻿using UnityEngine;
using System.Collections;
using VRTK;
using System;

public class EventHandler : MonoBehaviour
{
    #region Public Fields

    // pointer[0] = teleport
    // pointer[1] = UI


    public GameObject leftScripts;
    public GameObject rightScripts;


    protected VRTK_Pointer[] leftVrtkPointer = new VRTK_Pointer[2];
    protected VRTK_Pointer[] rightVrtkPointer = new VRTK_Pointer[2];

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        leftVrtkPointer = leftScripts.GetComponents<VRTK_Pointer>();
        rightVrtkPointer = rightScripts.GetComponents<VRTK_Pointer>();
        // Setup events for Teleport pointer
        leftVrtkPointer[0].ActivationButtonPressed += OnTeleportActivationButtonPressed;
        rightVrtkPointer[0].ActivationButtonPressed += OnTeleportActivationButtonPressed;

        leftVrtkPointer[0].ActivationButtonReleased += OnTeleportActivationButtonReleased;
        rightVrtkPointer[0].ActivationButtonReleased += OnTeleportActivationButtonReleased;
    }

    private void OnDisable()
    {
        leftVrtkPointer[0].ActivationButtonPressed -= OnTeleportActivationButtonPressed;
        rightVrtkPointer[0].ActivationButtonPressed -= OnTeleportActivationButtonPressed;

        leftVrtkPointer[0].ActivationButtonReleased -= OnTeleportActivationButtonReleased;
        rightVrtkPointer[0].ActivationButtonReleased -= OnTeleportActivationButtonReleased;
    }

    #endregion

    #region OnEvent

    // Allow just one teleport at the time
    private void OnTeleportActivationButtonPressed(object sender, ControllerInteractionEventArgs e)
    {
       
        if (e.controllerReference.actual.name == "RightHandAnchor")
        {
            rightVrtkPointer[1].enabled = false;
        }
        if (e.controllerReference.actual.name == "LeftHandAnchor")
        {
            leftVrtkPointer[1].enabled = false;
        }

        if (leftVrtkPointer[0].IsPointerActive())
        {
            leftVrtkPointer[0].currentActivationState = 0;
        }
        if (rightVrtkPointer[0].IsPointerActive())
        {
            rightVrtkPointer[0].currentActivationState = 0;
            
        }

        /*
         * 
         * TODO: ADD LIGHT TO THE HAND WHEN TELEPORTING NETWORKING
         * 
         */
    }

    private void OnTeleportActivationButtonReleased(object sender, ControllerInteractionEventArgs e)
    {
        if (e.controllerReference.actual.name == "RightHandAnchor")
        {
            rightVrtkPointer[1].enabled = true;
        }
        if (e.controllerReference.actual.name == "LeftHandAnchor")
        {
            leftVrtkPointer[1].enabled = true;
        }
    }


    #endregion
}

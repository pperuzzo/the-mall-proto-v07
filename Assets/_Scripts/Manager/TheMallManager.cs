﻿using UnityEngine;
using System.Collections;
using VRTK;
using Obi;

public class TheMallManager : MonoBehaviour
{
    #region Public Fields

    public GameObject leftHandScripts;
    public GameObject rightHandScripts;

    #endregion

    #region Protected Fields

    protected VRTK_InteractTouch leftInteractTouch;
    protected VRTK_InteractTouch rightInteractTouch;
    protected VRTK_InteractNearTouch leftInteractNearTouch;
    protected VRTK_InteractNearTouch rightInteractNearTouch;


    protected ProductData currentProduct = null;

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        leftInteractTouch = leftHandScripts.GetComponent<VRTK_InteractTouch>();
        rightInteractTouch = rightHandScripts.GetComponent<VRTK_InteractTouch>();

        leftInteractNearTouch = leftHandScripts.GetComponent<VRTK_InteractNearTouch>();
        rightInteractNearTouch = rightHandScripts.GetComponent<VRTK_InteractNearTouch>();

        SetupEvents();
    }

    private void OnDisable()
    {
        RemoveEvents();
    }

    #endregion

    #region Private Methods

    private void SetupEvents()
    {
        // Setup interact touch events
        if (leftInteractTouch != null)
        {
            leftInteractTouch.ControllerStartTouchInteractableObject += OnControllerStartTouchInteractableObject;
            leftInteractTouch.ControllerUntouchInteractableObject += OnControllerUntouchInteractableObject;
        }
        if (rightInteractTouch != null)
        {
            rightInteractTouch.ControllerStartTouchInteractableObject += OnControllerStartTouchInteractableObject;
            rightInteractTouch.ControllerUntouchInteractableObject += OnControllerUntouchInteractableObject;
        }

        // Setup interact near touch events
        if (leftInteractNearTouch != null)
        {
            leftInteractNearTouch.ControllerNearTouchInteractableObject += OnControllerNearTouchInteractableObject;
            leftInteractNearTouch.ControllerNearUntouchInteractableObject += OnControllerNearUntouchInteractableObject;
        }
        if (rightInteractNearTouch != null)
        {
            rightInteractNearTouch.ControllerNearTouchInteractableObject += OnControllerNearTouchInteractableObject;
            rightInteractNearTouch.ControllerNearUntouchInteractableObject += OnControllerNearUntouchInteractableObject;

        }

    }

    private void RemoveEvents()
    {
        // Remove interact touch events
        if (leftInteractTouch != null)
        {
            leftInteractTouch.ControllerStartTouchInteractableObject -= OnControllerStartTouchInteractableObject;
            leftInteractTouch.ControllerUntouchInteractableObject -= OnControllerUntouchInteractableObject;
        }
        if (rightInteractTouch != null)
        {
            rightInteractTouch.ControllerStartTouchInteractableObject -= OnControllerStartTouchInteractableObject;
            rightInteractTouch.ControllerUntouchInteractableObject -= OnControllerUntouchInteractableObject;
        }

        // Remove interact near touch events
        if (leftInteractNearTouch != null)
        {
            leftInteractNearTouch.ControllerNearTouchInteractableObject -= OnControllerNearTouchInteractableObject;
            leftInteractNearTouch.ControllerNearUntouchInteractableObject -= OnControllerNearUntouchInteractableObject;
        }
        if (rightInteractNearTouch != null)
        {
            rightInteractNearTouch.ControllerNearTouchInteractableObject -= OnControllerNearTouchInteractableObject;
            rightInteractNearTouch.ControllerNearUntouchInteractableObject -= OnControllerNearUntouchInteractableObject;

        }
    }

    #endregion

    #region On Event

    // Emitted when the touch of a valid object has started.
    private void OnControllerStartTouchInteractableObject(object sender, ObjectInteractEventArgs e)
    {
        if (e.target.GetComponent<ProductContainer>())
        {
            currentProduct = e.target.GetComponent<ProductContainer>().productData;
            e.target.GetComponentInChildren<ObiCloth>().enabled = true;
        }

    }

    // Emitted when a valid object is no longer being touched.
    private void OnControllerUntouchInteractableObject(object sender, ObjectInteractEventArgs e)
    {
        if (e.target.GetComponent<ProductContainer>())
        {
            currentProduct = null;
            e.target.GetComponentInChildren<ObiCloth>().enabled = false;
        }

    }

    // Emitted when a valid object is near touched.
    private void OnControllerNearTouchInteractableObject(object sender, ObjectInteractEventArgs e)
    {
        

    }

    // Emitted when a valid object is no longer being near touched.
    private void OnControllerNearUntouchInteractableObject(object sender, ObjectInteractEventArgs e)
    {
         
    }

    #endregion

}

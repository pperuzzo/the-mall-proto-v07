﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using static ProductDefinitions;

public class DressingRoomManager : MonoBehaviour
{

    #region Public Fields

    public SkinnedMeshRenderer avatarSkinnedMeshRenderer;
    public Dictionary<PositionType, ProductData> currentlyWearingProducts = new Dictionary<PositionType, ProductData>();
    public static DressingRoomManager Instance;

    public AvatarData avatarData;

    public struct DressingEventArgs
    {
        public ProductData dataProduct;
        public string dataSize;
    }

    public delegate void DressingEventHandler(object sender, DressingEventArgs e);

    public event DressingEventHandler WearProductEvent;
    public event DressingEventHandler RemoveProductEvent;

   
    #endregion

    #region Monobehaviour Callbacks

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        avatarData = VRTK.VRTK_SharedMethods.FindEvenInactiveComponent<AvatarContainer>().avatarData;
    }

    #endregion

    #region Public Methods

    // Called when pressing the delete button
    public void RemoveProduct(ProductData product)
    {
        currentlyWearingProducts.Remove(product.PositionType);
        OnRemoveProduct(SetupCartEventArgs(product));
        Destroy(product.InstantiatedGameObject);
    }

    // Called when pressing the wear button
    public void ManagerWearProduct(ProductData product, int size)
    {
        // Check if the new product to be weared is in the same position of an already worned product.
        if (currentlyWearingProducts.TryGetValue(product.PositionType, out ProductData toBeSubstitutedProduct))
        {
            RemoveProduct(toBeSubstitutedProduct);
            toBeSubstitutedProduct.CartButtonReference.forceButton = false;
            toBeSubstitutedProduct.CartButtonReference.UpdateButton();
            WearProduct(product, size);
        }
        else if (currentlyWearingProducts.Count >= 1)
        {
            OnRemoveProduct(SetupCartEventArgs(product));
            WearProduct(product, size);
        }
        else
        {
            WearProduct(product, size);
        }
    }



    public void WearDifferentSize(ProductData product, int size)
    {
        Destroy(product.InstantiatedGameObject);
        GameObject productGO = Instantiate(product.ToWearGameObject[size], Vector3.zero, Quaternion.identity);
        product.InstantiatedGameObject = productGO;

        SkinnedMeshRenderer[] skinnedMeshRenderers = productGO.GetComponentsInChildren<SkinnedMeshRenderer>();

        foreach (SkinnedMeshRenderer item in skinnedMeshRenderers)
        {
            Equipmentizer itemEquipmentizer = item.gameObject.AddComponent<Equipmentizer>();
            itemEquipmentizer.targetRenderer = avatarSkinnedMeshRenderer;
            itemEquipmentizer.Equip();
            itemEquipmentizer = null;
        }

    }

    public DressingEventArgs SetupCartEventArgs(ProductData product = null, string size = null)
    {
        DressingEventArgs e = new DressingEventArgs
        {
            dataProduct = product,
            dataSize = size
        };
        return e;
    }

    #endregion

    #region Private Methods

    private void WearProduct(ProductData product, int size)
    {
        currentlyWearingProducts.Add(product.PositionType, product);

        OnWearProduct(SetupCartEventArgs(product, size.ToString()));

        GameObject productGO = Instantiate(product.ToWearGameObject[size], Vector3.zero, Quaternion.identity);

        product.InstantiatedGameObject = productGO;

        SkinnedMeshRenderer[] skinnedMeshRenderers = productGO.GetComponentsInChildren<SkinnedMeshRenderer>();

        foreach (SkinnedMeshRenderer item in skinnedMeshRenderers)
        {
            Equipmentizer itemEquipmentizer = item.gameObject.AddComponent<Equipmentizer>();
            itemEquipmentizer.targetRenderer = avatarSkinnedMeshRenderer;
            itemEquipmentizer.Equip();
            itemEquipmentizer = null;
        }

    }

    #endregion

    #region OnEvent

    public void OnWearProduct(DressingEventArgs e)
    {
        if (WearProductEvent != null)
        {
            WearProductEvent(this, e);
        }
    }

    public void OnRemoveProduct(DressingEventArgs e)
    {
        if (RemoveProductEvent != null)
        {
            RemoveProductEvent(this, e);
        }
    }


    #endregion


}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cart : Singleton<Cart>
{
    #region Public Fields

    /*
    * 
    * TODO: Quantity adjustment for products when adding/removing
    * 
    */

    public struct CartEventArgs
    {
        public ProductData data;
    }

    public delegate void CartEventHandler(object sender, CartEventArgs e);

    public event CartEventHandler AddToCartEvent;
    public event CartEventHandler RemoveFromCartEvent;
    public event CartEventHandler UpdateCartEvent;
   

    public ProductData[] testingProducts = new ProductData[2];
    public Dictionary<string, ProductData> productsInCart = new Dictionary<string, ProductData>();

    public bool firstTime = true;
    #endregion

    #region MonoBehaviour Callbacks

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (firstTime)
            {
                foreach (ProductData item in testingProducts)
                {
                    AddToCart(item);
                }
                firstTime = false;
            }

        }
    }

    #endregion

    #region Public Methods

    public void AddToCart(ProductData product)
    {
        if (!productsInCart.ContainsKey(product.Id.ToString()))
        {
            productsInCart.Add(product.Id.ToString(), product);
            OnAddToCart(SetupCartEventArgs(product));
        }

    }

    public void RemoveFromCart(ProductData product)
    {
        if (productsInCart.ContainsKey(product.Id.ToString()))
        {
            productsInCart.Remove(product.Id.ToString());
            OnRemoveFromCart(SetupCartEventArgs(product));
        }
        
    }

    public void UpdateCart()
    {
        OnUpdateCart(SetupCartEventArgs());
    }

    public CartEventArgs SetupCartEventArgs(ProductData product = null)
    {
        CartEventArgs e = new CartEventArgs
        {
            data = product
        };
        return e;
    }

    #endregion

    #region OnEvent

    public void OnAddToCart(CartEventArgs e)
    {
        if (AddToCartEvent != null)
        {
            AddToCartEvent(this, e);
        }
    }

    public void OnRemoveFromCart(CartEventArgs e)
    {
        if (RemoveFromCartEvent != null)
        {
            RemoveFromCartEvent(this, e);
        }
    }

    public void OnUpdateCart(CartEventArgs e)
    {
        if (UpdateCartEvent != null)
        {
            UpdateCartEvent(this, e);
        }
    }


    #endregion
}

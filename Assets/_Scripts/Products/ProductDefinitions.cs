﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class ProductDefinitions

{
    public enum Gender
    {
        Woman = 0,
        Men = 1,
        Unisex = 2
    }

    public enum ProdType
    {
        TShirt = 0,
        Pants = 1,
        Watch = 2,
        Glasses = 3,
        Hat = 4,
        Shoe = 5,
        Chain = 6,
        Bracelet = 7
    }

    [Flags]
    public enum PositionType
    {
        Head = 0,
        Chest = 1,
        Hand = 2,
        Feet = 3,
        Finger = 4,
        Neck = 5,
        Wrist = 6,
        Legs = 7

    }


}

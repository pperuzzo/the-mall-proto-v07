﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Oculus.Platform;
using Oculus.Platform.Models;
using System;
using ExitGames.Client.Photon;

public class Loader : MonoBehaviourPunCallbacks
{
    #region Private Serializable Fields

    /// <summary>
    /// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
    /// </summary>
    [Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
    [SerializeField]
    private byte maxPlayersPerRoom = 10;


    #endregion

    #region Private Fields

    /// <summary>
    /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
    /// </summary>
    string gameVersion = "1";


    #endregion

    #region Public Fields

    public GameObject loadingScreen;

    public Transform mainCamera;

    public float offsetLoading = 0f;

    /// <summary>
    /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon,
    /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
    /// Typically this is used for the OnConnectedToMaster() callback.
    /// </summary>
    public bool isConnecting;

    #endregion

    #region MonoBehaviour CallBacks

    void Awake()
    {
        GetLoggedInUserInfo();
        // #Critical
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    private void Update()
    {
        loadingScreen.transform.position = new Vector3(loadingScreen.transform.position.x, mainCamera.position.y - offsetLoading, loadingScreen.transform.position.z);
    }


    #endregion

    #region Private Methods

    private void GetLoggedInUserInfo()
    {
        Core.Initialize();
        Users.GetLoggedInUser().OnComplete(OnLoggedInUserCallback);
        Request.RunCallbacks();  //avoids race condition with OvrAvatar.cs Start().
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Start the connection process.
    /// - If already connected, we attempt joining a random room
    /// - if not yet connected, Connect this application instance to Photon Cloud Network
    /// </summary>
    public void Connect()
    {

        // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
        isConnecting = true;

        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.NickName = LocalUserData.localUserID.ToString();
            // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            PhotonNetwork.NickName = LocalUserData.localUserID.ToString();
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.GameVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }


    #endregion

    #region Photon Callbacks

    public override void OnConnectedToMaster()
    {
        //Debug.Log("OnConnectedToMaster() was called by PUN");

        // we don't want to do anything if we are not attempting to join a room.
        // this case where isConnecting is false is typically when you lost or quit the game, when this level is loaded, OnConnectedToMaster will be called, in that case
        // we don't want to do anything.
        if (isConnecting)
        {
            // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
            PhotonNetwork.JoinRandomRoom();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        loadingScreen.SetActive(false);
        Debug.LogWarningFormat("OnDisconnected() was called by PUN: "+ cause);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\n Calling: PhotonNetwork.CreateRoom()");

        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions { MaxPlayers = maxPlayersPerRoom });
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom() was called by PUN. Now this client is in a room");
        // #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            Debug.Log("We load TheMall because we are the first player");

            if (PhotonNetwork.IsMasterClient)
            {
                // #Critical
                // Load the Room Level.
                PhotonNetwork.LoadLevel("TheMall");
            }
            
        }
        
    }

    #endregion

    #region Oculus Callbacks

    // Called after GetLoggedInUser retrived user data.
    private void OnLoggedInUserCallback(Message<User> msg)
    {
        if (msg.IsError)
        {
            Debug.Log(msg.GetError().Message);
        }
        else
        {
            LocalUserData.localUserID = msg.Data.ID;
            LocalUserData.localUserName = msg.Data.OculusID;

            Users.GetUserProof().OnComplete(OnUserProofCallback);
        }
    }

    // Called after GetUserProof retrieved the nonce.
    private void OnUserProofCallback(Message<UserProof> msg)
    {
        if (msg.IsError)
        {
            Debug.Log(msg.GetError().Message);
        }
        else
        {
            string oculusNonce = msg.Data.Value;
            LoadBalancingClient loadBalancingClient = new LoadBalancingClient();
            loadBalancingClient.AuthValues = new AuthenticationValues();
            loadBalancingClient.AuthValues.AuthType = CustomAuthenticationType.Oculus;
            loadBalancingClient.AuthValues.AddAuthParameter("userid", LocalUserData.localUserID.ToString());
            loadBalancingClient.AuthValues.AddAuthParameter("nonce", oculusNonce);

            // Start the connection process.
            Connect();
        }
    }

    #endregion
}

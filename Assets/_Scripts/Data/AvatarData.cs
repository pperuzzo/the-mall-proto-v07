﻿#pragma warning disable 0649
using UnityEngine;

[CreateAssetMenu(fileName = "AvatarData", menuName = "Data/AvatarData", order = 2)]
public class AvatarData : ScriptableObject
{
    #region Private Fields

    [Header("SIZES")]
    [SerializeField]
    private float height;
    public float Height
    {
        get
        {
            return height;
        }
    }
    [SerializeField]
    private float hips;
    public float Hips
    {
        get
        {
            return hips;
        }
    }
    [SerializeField]
    private float waist;
    public float Waist
    {
        get
        {
            return waist;
        }
    }
    [SerializeField]
    private float thigh;
    public float Thigh
    {
        get
        {
            return thigh;
        }
    }
    [SerializeField]
    private float inseamHeight;
    public float InseamHeight
    {
        get
        {
            return inseamHeight;
        }
    }
    [SerializeField]
    private float chest;
    public float Chest
    {
        get
        {
            return chest;
        }
    }

    #endregion
}

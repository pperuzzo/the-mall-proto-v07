﻿#pragma warning disable 0649
using UnityEngine;
using static ProductDefinitions;

[CreateAssetMenu(fileName = "ProductData", menuName = "Data/ProductData", order = 2)]
public class ProductData : ScriptableObject
{
    #region Private Fields

    [Header("GENERAL")]
    [SerializeField]
    private int id;
    public int Id
    {
        get
        {
            return id;
        }
    }
    [SerializeField]
    private string price;
    public string Price
    {
        get
        {
            return price;
        }
    }
    [SerializeField]
    private string material;
    public string Material
    {
        get
        {
            return material;
        }
    }
    [SerializeField]
    private string brand;
    public string Brand
    {
        get
        {
            return brand;
        }
    }
    [SerializeField]
    private int brand_Id;
    public int Brand_Id
    {
        get
        {
            return brand_Id;
        }
    }
    [SerializeField]
    private string title;
    public string Title
    {
        get
        {
            return title;
        }
    }
    [SerializeField]
    private string description;
    public string Description
    {
        get
        {
            return description;
        }
    }
    [SerializeField]
    private Gender productGender;
    public Gender ProductGender
    {
        get
        {
            return productGender;
        }
    }
    [SerializeField]
    private ProdType productType;
    public ProdType ProductType
    {
        get
        {
            return productType;
        }
    }
    [SerializeField]
    private PositionType positionType;
    public PositionType PositionType
    {
        get
        {
            return positionType;
        }
    }
    [SerializeField]
    private Sprite productImage;
    public Sprite ProductImage
    {
        get
        {
            return productImage;
        }
    }
    [SerializeField]
    [Tooltip("If position type is chest: Element 0: S, Element 1: M, Element 2: L")]
    private GameObject[] toWearGameObject;
    public GameObject[] ToWearGameObject
    {
        get
        {
            return toWearGameObject;
        }
        set
        {
            toWearGameObject = value;
        }
    }
    [SerializeField]
    private Material[] productMaterials;
    public Material[] ProductMaterials
    {
        get
        {
            return productMaterials;
        }
    }
    [SerializeField]
    private Color[] productColorVariants;
    public Color[] ProductColorVariants
    {
        get
        {
            return productColorVariants;
        }
    }
    [SerializeField]
    private GameObject instantiatedGameObject;
    public GameObject InstantiatedGameObject
    {
        get
        {
            return instantiatedGameObject;
        }
        set
        {
            instantiatedGameObject = value;
        }
    }
    [SerializeField]
    private CartWearButton cartButtonReference;
    public CartWearButton CartButtonReference
    {
        get
        {
            return cartButtonReference;
        }
        set
        {
            cartButtonReference = value;
        }
    }
    #endregion
}

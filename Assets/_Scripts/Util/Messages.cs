﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Messages
{
    public static Dictionary<string, string> messageType = new Dictionary<string, string>()
    {
        { "Connection Error", "There was an error with your connection. Please check to be connected to a network and retry." }
    };


}

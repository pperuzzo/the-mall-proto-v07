﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipmentizer : MonoBehaviour
{
    #region Public Fields

    public SkinnedMeshRenderer targetRenderer;


    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        // To test
        //Equip();
    }

    #endregion

    #region Public Methods

    public void Equip()
    {

        Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();

        // Popola dizionario con key = nome del bone all'interno del target (maglietta), value = bone (transform)
        foreach (Transform bone in targetRenderer.bones) boneMap[bone.gameObject.name] = bone;

        SkinnedMeshRenderer myRenderer = gameObject.GetComponent<SkinnedMeshRenderer>();

        Transform[] newBones = new Transform[myRenderer.bones.Length];
        for (int i = 0; i < myRenderer.bones.Length; ++i)
        {
            GameObject bone = myRenderer.bones[i].gameObject;
            if (!boneMap.TryGetValue(bone.name, out newBones[i]))
            {
                Debug.Log("Unable to map bone \"" + bone.name + "\" to target skeleton.");
                break;
            }
        }

        myRenderer.bones = newBones;
    }

    #endregion

}
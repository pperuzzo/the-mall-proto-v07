﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;
using static ProductDefinitions;
using System.Data;

public class SizeCalculator
{

    #region Private Methods

    private bool IsInBetween(float numberToCheck, float bottom, float top)
    {
        return (numberToCheck >= bottom && numberToCheck <= top);
    }

    private int ChestSize(float chestSize, IDataReader reader)
    {

        while (reader.Read())
        {
            float bottom, top;
            string[] exploded = reader[5].ToString().Split('-');
            bottom = float.Parse(exploded[0]);
            top = float.Parse(exploded[1]);
            if (IsInBetween(chestSize, bottom, top))
            {
                Int32.TryParse(reader[4].ToString(), out int toReturnValue);
                return toReturnValue;
            }

        }
        return 0;
    }

    private int WaistSize(float waistSize, IDataReader reader)
    {
        while (reader.Read())
        {
            float bottom, top;
            string[] exploded = reader[5].ToString().Split('-');
            bottom = float.Parse(exploded[0]);
            top = float.Parse(exploded[1]);
            if (IsInBetween(waistSize, bottom, top))
            {
                Int32.TryParse(reader[4].ToString(), out int toReturnValue);
                return toReturnValue;
            }

        }
        return 0;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Compares the chest size of the brand and of the avatar. Returns an int that which is the key for the array of the different sizes models
    /// 0 = S
    /// 1 = M
    /// 2 = L
    /// </summary>
    public int RecommendSize(AvatarData avatarData, ProductData productData)
    {
        SqliteHelper helper = new SqliteHelper();
        IDataReader reader = helper.GetDataForSizes(productData);

        switch (productData.ProductType)
        {
            case ProdType.TShirt:
                return ChestSize(avatarData.Chest, reader);
            case ProdType.Pants:
                return WaistSize(avatarData.Waist, reader);
            case ProdType.Watch:
                break;
            case ProdType.Glasses:
                break;
            case ProdType.Hat:
                break;
            case ProdType.Shoe:
                break;
            case ProdType.Chain:
                break;
            case ProdType.Bracelet:
                break;
            default:
                break;
        }
        return 0;
    }

    public string ConvertToInches()
    {
        // TODO: convert cm into inches to display
        return "";
    }

    #endregion

}
